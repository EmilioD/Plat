# frozen_string_literal: true

class CreateMains < ActiveRecord::Migration[7.0]
  def change
    create_table :mains, &:timestamps
  end
end
